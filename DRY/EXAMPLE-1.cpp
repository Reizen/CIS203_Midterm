#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	int x = 5;
	
	cout << x << " to the power of " << 1 << " = " << pow(x, 1) << endl;
	cout << x << " to the power of " << 2 << " = " << pow(x, 2) << endl;
	cout << x << " to the power of " << 3 << " = " << pow(x, 3) << endl;
	cout << x << " to the power of " << 4 << " = " << pow(x, 4) << endl;
	cout << x << " to the power of " << 5 << " = " << pow(x, 5) << endl;

	return 0;
}