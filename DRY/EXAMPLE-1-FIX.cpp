#include <iostream>
#include <cmath>

using namespace std;

void printPower(int, int);

int main()
{
	int x = 5;
	
	for (int i = 1; i <= 5; i++)
		printPower(x, i);

	return 0;
}

void printPower(int x, int y)
{
	cout << x << " to the power of " << y << " = " << pow(x, y) << endl;	
}